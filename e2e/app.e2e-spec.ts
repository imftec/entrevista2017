import { EntrevistaPage } from './app.po';

describe('entrevista App', () => {
  let page: EntrevistaPage;

  beforeEach(() => {
    page = new EntrevistaPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
