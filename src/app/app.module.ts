import { TodoService } from './todo.service';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { AppComponent } from './app.component';

export const firebaseconfig = {
  apiKey: "AIzaSyDi-1BPsmn_Rd3F0jAwbxT3_dVBqtrehf8",
  authDomain: "entrevista-getconnect.firebaseapp.com",
  databaseURL: "https://entrevista-getconnect.firebaseio.com",
  projectId: "entrevista-getconnect",
  storageBucket: "",
  messagingSenderId: "327316696899"
};

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AngularFireModule.initializeApp(firebaseconfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
