import { Todo } from './todo';
import { Injectable } from '@angular/core';

import { FirebaseListObservable, FirebaseObjectObservable, AngularFireDatabase } from 'angularfire2/database';

@Injectable()
export class TodoService {

  todos: FirebaseListObservable<Todo>;

  constructor(
    private firebase: AngularFireDatabase
  ) {
    this.listTodos();
  }

  /**
   * Cadastra uma nova tarefa no banco de dados
   * 
   * @param todo
   */
  createTodo( todo: string ) {
    this.todos.push(todo);
  }

  /**
   * Lista todas as tarefas do banco de dados
   */
  listTodos() {
    this.todos = this.firebase.list('/todos');
    this.todos.subscribe(todo => {
      console.log(todo);
    })
  }

}
